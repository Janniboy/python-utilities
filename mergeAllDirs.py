#!/usr/bin/env python
import argparse
from pathlib import Path
import os
import shutil

def listDirNames(path):
	items = os.listdir(path)
	dirs = []
	for i in items:
		if os.path.isdir(os.path.join(path, i)):
			dirs.append(i)
	return dirs

# blatantly copied https://lukelogbook.tech/2018/01/25/merging-two-folders-in-python/
def mergefolders(root_src_dir, root_dst_dir):
    for src_dir, dirs, files in os.walk(root_src_dir):
        dst_dir = src_dir.replace(root_src_dir, root_dst_dir, 1)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                os.remove(dst_file)
            shutil.move(src_file, dst_dir)


parser = argparse.ArgumentParser(description="merge all subdirectories of PATH into (new) subdirectory (of PATH) NAME")
parser.add_argument("name", metavar="NAME", type=str, nargs=1, help="name of the new directory")
parser.add_argument("path", metavar="PATH", type=str, nargs=1, help="path to the parent directory")
parser.add_argument("-k", "--keep-empty", action="store_true", help="keep empty directories")
args = parser.parse_args()

path = args.path[0]
new_dir = args.name[0]
sub_dirs = []

# convert str to path
path = Path(path)

# validate path (directory exists and contains directories)
if not os.path.isdir(path):
    raise argparse.ArgumentTypeError("Directory does not exist.")
sub_dirs = listDirNames(path)
if len(sub_dirs)<=1:
    raise argparse.ArgumentTypeError("Directory has 1 or less sub directories.")

# remove new_dir from sub_dirs
if new_dir in sub_dirs:
	sub_dirs.remove(new_dir)
new_dir = os.path.join(path, new_dir)

# create new_dir (if it doesn't exist already)
if not os.path.isdir(new_dir):
	os.mkdir(new_dir)

# main
for dirstr in sub_dirs:
	dir = os.path.join(path, dirstr)
	items = os.listdir(dir)
	for i in items:
		ipath = os.path.join(dir, i)
		npath = os.path.join(new_dir, i)
		if os.path.isdir(ipath):
			mergefolders(ipath, npath)
		else:
			try:
				os.rename(ipath, npath)
			except FileExistsError:
				print(f"Couldn't move: {ipath}")

# delete empty
finished = False
while not finished:
	finished = True
	if not args.keep_empty:
		for d, ds, fs in os.walk(path, topdown=False):
			if len(ds+fs) == 0:
				if os.path.isdir(d):
					finished = False
					os.rmdir(d)
				else:
					finished = False
					os.remove(d)