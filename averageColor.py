#!/usr/bin/env python
from PIL import Image, UnidentifiedImageError
import argparse
from pathlib import Path

def imagePath(s: str):
    p = Path(s)
    if not p.is_file():
        raise argparse.ArgumentTypeError(f"\"{s}\" does not exist or is not a file.")
    try:
        img = Image.open(p)
        return img
    except UnidentifiedImageError:
        raise argparse.ArgumentTypeError(f"\"{p}\" is not an image")

parser = argparse.ArgumentParser(description="Reads an image and returns the average color of it.")
parser.add_argument("image", metavar="IMG", type=imagePath, help="path to the image")
args = parser.parse_args()

img = args.image

r, g, b = (0,0,0)
c = 0

for y in range(img.height):
    for x in range(img.width):
        c = c + 1
        a = img.getpixel((x, y))
        r += a[0]
        g += a[1]
        b += a[2]

print(f"({round(r/c)},{round(g/c)},{round(b/c)})")
