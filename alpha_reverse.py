#!/usr/bin/env python
# TODO:
# - performance optimisation
from argparse import ArgumentError
from imghdr import tests
from math import floor, inf

ITERATE_ORDER = [128.0, 64.0, 192.0, 32.0, 96.0, 160.0, 224.0, 16.0, 48.0, 80.0, 112.0, 144.0, 176.0, 208.0, 240.0, 8.0, 24.0, 40.0, 56.0, 72.0, 88.0, 104.0, 120.0, 136.0, 152.0, 168.0, 184.0, 200.0, 216.0, 232.0, 248.0, 4.0, 12.0, 20.0, 28.0, 36.0, 44.0, 52.0, 60.0, 68.0, 76.0, 84.0, 92.0, 100.0, 108.0, 116.0, 124.0, 132.0, 140.0, 148.0, 156.0, 164.0, 172.0, 180.0, 188.0, 196.0, 204.0, 212.0, 220.0, 228.0, 236.0, 244.0, 252.0, 2.0, 6.0, 10.0, 14.0, 18.0, 22.0, 26.0, 30.0, 34.0, 38.0, 42.0, 46.0, 50.0, 54.0, 58.0, 62.0, 66.0, 70.0, 74.0, 78.0, 82.0, 86.0, 90.0, 94.0, 98.0, 102.0, 106.0, 110.0, 114.0, 118.0, 122.0, 126.0, 130.0, 134.0, 138.0, 142.0, 146.0, 150.0, 154.0, 158.0, 162.0, 166.0, 170.0, 174.0, 178.0, 182.0, 186.0, 190.0, 194.0, 198.0, 202.0, 206.0, 210.0, 214.0, 218.0, 222.0, 226.0, 230.0, 234.0, 238.0, 242.0, 246.0, 250.0, 254.0, 1, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0, 21.0, 23.0, 25.0, 27.0, 29.0, 31.0, 33.0, 35.0, 37.0, 39.0, 41.0, 43.0, 45.0, 47.0, 49.0, 51.0, 53.0, 55.0, 57.0, 59.0, 61.0, 63.0, 65.0, 67.0, 69.0, 71.0, 73.0, 75.0, 77.0, 79.0, 81.0, 83.0, 85.0, 87.0, 89.0, 91.0, 93.0, 95.0, 97.0, 99.0, 101.0, 103.0, 105.0, 107.0, 109.0, 111.0, 113.0, 115.0, 117.0, 119.0, 121.0, 123.0, 125.0, 127.0, 129.0, 131.0, 133.0, 135.0, 137.0, 139.0, 141.0, 143.0, 145.0, 147.0, 149.0, 151.0, 153.0, 155.0, 157.0, 159.0, 161.0, 163.0, 165.0, 167.0, 169.0, 171.0, 173.0, 175.0, 177.0, 179.0, 181.0, 183.0, 185.0, 187.0, 189.0, 191.0, 193.0, 195.0, 197.0, 199.0, 201.0, 203.0, 205.0, 207.0, 209.0, 211.0, 213.0, 215.0, 217.0, 219.0, 221.0, 223.0, 225.0, 227.0, 229.0, 231.0, 233.0, 235.0, 237.0, 239.0, 241.0, 243.0, 245.0, 247.0, 249.0, 251.0, 253.0, 255.0]

def _apply_overlay(base, overlay):
    t = overlay[3] / 255
    t2 = (255 - overlay[3]) / 255
    return floor(base[0]*t2  + overlay[0]*t), floor(base[1]*t2 + overlay[1]*t), floor(base[2]*t2 + overlay[2]*t)

def _is_valid(p):
    for v in p:
        if v < 0 or v > 255:
            return False 
    return True

def _get_inaccuracy(base, sumd, calc_overlay) -> float:
    """ returns a value from 0 to 1, with 0 being the most accurate """
    if not _is_valid(calc_overlay):
        return 1
    calc_sumd = _apply_overlay(base, calc_overlay)
    avg_diff = ( abs(sumd[0] - calc_sumd[0]) + abs(sumd[1] - calc_sumd[1]) + abs(sumd[2] - calc_sumd[2]) ) / 3
    return avg_diff / 255

def get_overlay(base, sumd):
    """ When given a sumd pixel to which a transparent overlay was applied and what the pixel was before (base), this function returns an rgba pixel that would work as an overlay to produce the (almost) same sumd pixel """
    if base == sumd:
        return 0, 0, 0, 0
    
    # calculate the SB values for a channels
    SBr = 255 * ( sumd[0] - base[0] )
    SBg = 255 * ( sumd[1] - base[1] )
    SBb = 255 * ( sumd[2] - base[2] )
    
    # calculate alpha candidates from all channels
    tr = SBr / (255 - base[0]) if base[0] != 255 else inf
    tg = SBg / (255 - base[1]) if base[1] != 255 else inf
    tb = SBb / (255 - base[2]) if base[2] != 255 else inf
    
    # use alpha candidates to calulate overlay candidates
    outR = ( (SBr / tr) + base[0], (SBg / tr) + base[1], (SBb / tr) + base[2], tr ) if tr != 0 else (inf, inf, inf, inf)
    outG = ( (SBr / tg) + base[0], (SBg / tg) + base[1], (SBb / tg) + base[2], tg ) if tg != 0 else (inf, inf, inf, inf)
    outB = ( (SBr / tb) + base[0], (SBg / tb) + base[1], (SBb / tb) + base[2], tb ) if tb != 0 else (inf, inf, inf, inf)

    # get accuracy for all overlay candidates
    aR = _get_inaccuracy(base, sumd, outR)
    aG = _get_inaccuracy(base, sumd, outG)
    aB = _get_inaccuracy(base, sumd, outB)

    # choose overlay candidate with highest accuracy
    minAcc = min(aR, aG, aB)
    if minAcc == aR and _is_valid(outR):
        return outR[0], outR[1], outR[2], tr
    elif minAcc == aG and _is_valid(outG):
        return outG[0], outG[1], outG[2], tg
    elif minAcc == aB and _is_valid(outB):
        return outB[0], outB[1], outB[2], tb
    # if there is no elegible candidate, brute-force search instead
    for i in ITERATE_ORDER:
        out = ( (SBr / i) + base[0], (SBg / i) + base[1], (SBb / i) + base[2], i )
        if _is_valid(out):
            return out
    return 0,0,0,0

def unit_test():
    print("running tests")
    import random
    def random_rgb():
        return random.randint(0,255), random.randint(0,255), random.randint(0,255)

    def random_rgba():
        return random.randint(0,255), random.randint(0,255), random.randint(0,255), random.randint(0,255)

    def test():
        base = random_rgb()
        overlay = random_rgba()
        sumd = _apply_overlay(base, overlay)
        calc_overlay = get_overlay(base, sumd)
        calc_sumd = _apply_overlay(base, calc_overlay)
        avg_diff = abs(sumd[0] - calc_sumd[0]) + abs(sumd[1] - calc_sumd[1]) + abs(sumd[2] - calc_sumd[2])
        avg_diff = avg_diff / 3
        return avg_diff / 255, f"b{base} o{overlay} s{sumd}"

    worst = 0
    worst_text = ""
    sum = 0
    i = 0

    for i in range(0,10000):
        w, wt = test()
        sum += w
        i+=1
        if w > worst:
            worst = w
            worst_text = wt

    print(f"The worst accuracy was: %f%%, here its description: %s" % ((1-worst)*100, worst_text))
    print(f"The average accuracy was: %f%%" % ((1-(sum/i)) * 100))

## TESTS
if __name__ == "__main__":
    import sys # debug
    sys.argv = [sys.argv[0], "C:\\Users\\Jann\\Desktop\\combiner\\005.jpg", "C:\\Users\\Jann\\Desktop\\combiner\\005.jpg", "out.jpg"]
    import argparse
    from PIL import Image, UnidentifiedImageError
    from pathlib import Path
    import numpy as np
    class test_action(argparse.Action):
        def __init__(self, option_strings, dest, **kw_args):
            super().__init__(option_strings, dest, nargs=0, default=argparse.SUPPRESS, **kw_args)
        def __call__(self, parser, namespace, values, option_string, **kw_args):
            unit_test()
            parser.exit()

    def imagePath(s: str):
        p = Path(s)
        if not p.is_file():
            raise argparse.ArgumentTypeError(f"\"{s}\" does not exist or is not a file.")
        try:
            img = Image.open(p)
            return img
        except UnidentifiedImageError:
            raise argparse.ArgumentTypeError(f"\"{p}\" is not an image")

    def outputPath(s: str):
        p = Path(s).absolute().resolve()
        if p.is_dir():
            raise argparse.ArgumentTypeError(f"\"{s}\" is a directory")
        if not p.parent.exists():
            raise argparse.ArgumentTypeError(f"\"{str(p.parent)}\" does not exist.")
        return str(p)

    parser = argparse.ArgumentParser(description="This tries to calculate a partially transparent overlay given a base image and the base image with an overlay applied to it.")
    parser.add_argument("-t", "--test", action=test_action, help="run tests instead of calculating the overlay")
    parser.add_argument("base_image", type=imagePath, help="name of the base image")
    parser.add_argument("sumd_image", type=imagePath, help="name of the base image with the overlay on top")
    parser.add_argument("output", type=outputPath, help="output path for a png file")
    args = parser.parse_args()
    # check sizes are equal
    base = args.base_image
    sumd = args.sumd_image
    if base.width != sumd.width:
        raise ArgumentError("base_image and sumd_image are not the same size")
    elif base.height != sumd.height:
        raise ArgumentError("base_image and sumd_image are not the same size")
    # iterate over all pixels and calculate new ones
    outp = np.reshape([0]*(base.height*base.width*4), (base.height, base.width, 4))
    for x in range(base.width):
        for y in range(base.height):
            ov = get_overlay(base.getpixel((x, y)), sumd.getpixel((x,y)))
            outp[y][x][0] = ov[0]
            outp[y][x][1] = ov[1]
            outp[y][x][2] = ov[2]
            outp[y][x][3] = ov[3]
    # writeout
    img = Image.fromarray(outp, "RGBA")
    img.save(args.output, "png")