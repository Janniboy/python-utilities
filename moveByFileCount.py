#!/usr/bin/env python
import os
from pathlib import Path
import argparse

def directory1(s: str):
    p = Path(s).resolve()
    if not p.exists():
        raise argparse.ArgumentTypeError(f"Path \"{s}\" does not exist")
    elif not p.is_dir():
        raise argparse.ArgumentTypeError(f"\"{s}\" is not a directory")
    return p

parser = argparse.ArgumentParser(description="Moves subdirectories of 'path' to 'tpath', if the number of files contained is equal to or lower than 'max' and the directories name is not included in 'excludes'")
parser.add_argument("path", metavar="path", type=directory1, help="parent directory, absolute")
parser.add_argument("npath", metavar="tpath", type=str, help="target directory, relative to the parent directory. If it resolves to be inside the parent directory, the according entry is added to 'excludes'")
parser.add_argument("max", type=int, help="maximum number of files a directory should contain if it is to be moved")
parser.add_argument("excludes", nargs="*", help="names of directories that shouldn't be moved")
args = parser.parse_args()

# inputs
path = args.path
max_number = args.max
excludes = args.excludes

# validate npath
npath = Path(os.path.join(args.path, args.npath)).resolve()
if not npath.is_dir():
    raise argparse.ArgumentError(f"\"{npath}\" does not exist or is not a directory")
if npath.is_relative_to(args.path):
    excludes.append(npath.relative_to(args.path).parts[0])

def count(p):
    c = 0
    for _, _, fs in os.walk(p):
        c += len(fs)
    return c

# get directories
items = os.listdir(path)

for i in items:
    p = Path(os.path.join(path, i))
    if not i in excludes and os.path.isdir(p):
        if count(p) <= max_number:
            p2 = Path(os.path.join(npath, p.name)).absolute().resolve()
            os.rename(p, p2)
