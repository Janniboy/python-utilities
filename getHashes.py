import sys
from pathlib import Path
import os
import hashlib
import bisect

parentDir = None

if len(sys.argv) > 1:
    parentDir = Path(sys.argv[1]).absolute()
    if not parentDir.is_dir():
        print("First Commandline Argument must be a valid path")
        exit()
else:
    parentDir = Path(".").absolute()

outfile = parentDir.joinpath("hashes.txt").absolute()
if outfile.exists():
        print(f"The file {outfile.relative_to(parentDir)} already exists")
        exit()

print(f"Parent Dir: \"{parentDir}\"")

hashes = []
dupes = []

with open(outfile, "x") as f:
    for root, dir_names, file_names in os.walk(parentDir):
        for file_name in file_names:
            path = parentDir.joinpath(root).joinpath(file_name).absolute()
            if path.samefile(outfile):
                continue
            line = ""
            if path.lstat().st_size == 0:
                line = f"Skipped empty file: \"{path.relative_to(parentDir)}\""
            else:
                hash = hashlib.md5(open(path, "rb").read()).hexdigest()
                bisect.insort(hashes, hash)
                line = f"{hash}: \"{path.relative_to(parentDir)}\""
            f.write(line+"\n")
            print(line)

    last_dupe = None
    for i in range(len(hashes)-1):
        curr = hashes[i]
        if curr == last_dupe:
            continue
        if curr == hashes[i+1]:
            last_dupe = curr
            dupes.append(curr)

    if len(dupes) > 0:
        print("\nDuplicates:")
        f.write("\nDuplicates:\n")
        for hash in dupes:
            print(hash)
            f.write(hash+"\n")
    else:
        print("Didn't find any duplicates (by hash)")